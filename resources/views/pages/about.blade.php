@extends('app')

@section('content')

<h1>About</h1>

@if (count($people))
<h3>People like:</h3>
<ul>
    @foreach($people as $person)
        <li> {{$person}}</li>
        @endforeach
</ul>
@endif

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rutrum libero quis lorem bibendum volutpat. Phasellus tristique, sem eget egestas pulvinar, risus sapien imperdiet metus, nec laoreet metus velit a purus. Vivamus sagittis metus non semper vehicula. Nulla erat felis, tempus ac dictum et, ultrices vel tellus</p>
@stop