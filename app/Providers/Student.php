<?php
/**
 * Created by PhpStorm.
 * User: krizmanicsnorbert
 * Date: 2018. 08. 28.
 * Time: 10:43
 */
namespace App;
use Illuminate\Database\Eloquent\Model;

class Student extends Model {

    protected $fillable = ['first_name', 'last_name'];
}