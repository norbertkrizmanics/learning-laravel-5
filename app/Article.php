<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = [
        'user_id',
        'title',
        'body',
        'excerpt',
        'user_id', //temoprary
        ];
    public function user(){
        //an article is owned by a user
        return $this->belongsTo('App\User');
    }
    public function tags(){
        return $this->belongsToMany('App\Tag');

    }
}
